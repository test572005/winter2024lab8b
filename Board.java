import java.util.Random;

public class Board{
	final int size = 5;
	private Tile[][] grid;
	
	public Board(){
		Random rng = new Random();
		grid = new Tile[size][size];
		for(int z = 0; z < size; z++){
			int toChange = rng.nextInt(grid[z].length);
			for(int n = 0; n < size; n++){
				grid[z][n] = Tile.BLANK;
			}
			grid[z][toChange] = Tile.HIDDEN_WALL;
		}
	}
	
	
	public String toString(){
		String board = "Board Game: \n";
		for(int z = 0; z < size; z++){
			for (int n = 0; n < size; n++){
				board += grid[z][n].getName() + " ";
			}
			board += "\n";
		}
		return board;
	}
	
	public int placeToken(int row, int col){
		if(row < 0 || row >= size || col < 0 || col >= size){ 
			return -2;
		}
		else if(grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL){
			return -1;
		}
		else if(grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		}
		else{
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
}