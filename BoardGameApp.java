import java.util.Scanner;

public class BoardGameApp{
	
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to the Board Game App");
		Board board = new Board();
		
		int numCastles = 5;
		int turns = 0;
		
		while(numCastles > 0 && turns < 8){
			System.out.println(board);
			System.out.println("Number of Castle: " + numCastles);
			System.out.println("Number of turns: " + turns);
			
			System.out.println("Place your token");
			int row = reader.nextInt();
			int col = reader.nextInt();
			
			int response = board.placeToken(row, col);
			
			if(response < 0){
				System.out.println("Invalid, please re-enter your row and column number");
			}
			else if(response == 1){
				System.out.println("There is a wall in that position. Turn added by 1");
				turns++;
			}
			else{
				System.out.println("A castle tile was successfully place. Turns added and Number of castle deducted by 1");
				turns++;
				numCastles--;
			}
		}	
		
		System.out.println(board);
		if(numCastles == 0){
			System.out.println("You won :)");
		}
		else{
			System.out.println("You lost :(");
		}
	}
}